from huaweicloudsdkcore.auth.credentials import BasicCredentials
from huaweicloudsdkdns.v2.region.dns_region import DnsRegion
from huaweicloudsdkdns.v2 import *


class Api:
    def __init__(self, ak, sk, region, current_name):
        # self.setting_dir = json.load(open("setting.json", encoding='utf8'))  # load setting.json
        self.region = region
        self.zone = {}
        self.zone_id = ''
        self.zone_name = ''
        self.current_name = current_name
        self.client = DnsClient.new_builder().with_credentials(
            BasicCredentials(
                ak,
                sk
            )
        ).with_region(
            DnsRegion.value_of(
                region
            )
        ).build()

        self.__init_get_zones()

    def __init_get_zones(self):
        request = ListPublicZonesRequest()
        response = self.client.list_public_zones(request)
        res = response.to_dict()["zones"]
        for r in res:
            if 'name' in r and r['name'] == self.current_name:
                self.zone = r
                self.zone_id = r['id']
                self.zone_name = r['name']
                break

    def list_record_sets(self):
        request = ListRecordSetsRequest()
        request.zone_id = self.zone_id
        response = self.client.list_record_sets(request)
        return response.to_dict()["recordsets"]

    def create_record_set(self, list_new_records, record_set_name, record_type, record_set_ttl=300):
        request = CreateRecordSetRequest()
        request.zone_id = self.zone_id
        request.body = CreateRecordSetReq(
            records=list_new_records,
            type=record_type,
            name=record_set_name,
            ttl=record_set_ttl
        )
        response = self.client.create_record_set(request)
        print(response)
        return response.to_dict()

    def update_record_set(self, list_update_records, record_name, record_type, record_id, record_set_ttl=300):
        request = UpdateRecordSetRequest()
        request.zone_id = self.zone_id
        request.recordset_id = record_id
        request.body = UpdateRecordSetReq(
            records=list_update_records,
            type=record_type,
            name=record_name,
            ttl=record_set_ttl,
        )
        response = self.client.update_record_set(request)
        return response.to_dict()
