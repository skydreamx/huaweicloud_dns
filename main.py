import json
from huaweicloudsdkcore.exceptions import exceptions
import sys
import Api
import logger


def get_local_ip():
    # logger.logger.info("Get local ip")
    from urllib.request import urlopen
    res = {}
    try:
        ip = str(urlopen('https://api.ipify.org/').read(), encoding='utf-8').strip()
        res.update({"A": ip})
    except Exception as ex:
        logger.logger.error(ex)
        logger.logger.error(ex.__traceback__.tb_lineno)

    try:
        ip = str(urlopen('https://api6.ipify.org/').read(), encoding='utf-8').strip()
        res.update({"AAAA": ip})
    except Exception as ex:
        logger.logger.error(ex)
        logger.logger.error(ex.__traceback__.tb_lineno)
        logger.logger.warning("IPv6 is not supported")

    return res


if __name__ == "__main__":
    setting_dir = json.load(open("setting.json", encoding='utf8'))  # load setting.json
    with open("logger.log", 'w+') as f:
        if len(f.readlines()) > 120:
            f.write("")

    try:
        dns_api = Api.Api(
            setting_dir['Access_Key_Id'],
            setting_dir['Secret_Access_Key'],
            setting_dir['Region'],
            setting_dir['current_name']
        )
        logger.logger.info("Start to create record set")
        local_ips = get_local_ip()
        local_v4 = local_ips['A'] if "A" in local_ips else None
        local_v6 = local_ips['AAAA'] if "AAAA" in local_ips else None

        # assert False
        created_v4 = False
        created_v6 = False
        for i in dns_api.list_record_sets():
            if i['name'] == setting_dir["current_record_set_A"] and i['type'] == "A":
                # logger.logger.info("record set A has been created")
                created_v4 = True
                record_set_id = i['id']
                dns_api.update_record_set(
                    record_id=record_set_id,
                    list_update_records=[local_v4],
                    record_name=setting_dir["current_record_set_A"],
                    record_type="A",
                ) if local_v4 else None
                # logger.logger.info("Record v4 set has been updated")
            elif i['name'] == setting_dir["current_record_set_AAAA"] and i['type'] == "AAAA":
                # logger.logger.info("record set AAAA has been created")
                created_v6 = True
                record_set_id = i['id']
                dns_api.update_record_set(
                    record_id=record_set_id,
                    list_update_records=[local_v6],
                    record_name=setting_dir["current_record_set_AAAA"],
                    record_type="AAAA"
                ) if local_v6 else None
                # logger.logger.info("Record v6 set has been updated")

        # 如果没有该记录集，则创建
        if not created_v4 and local_v4:
            dns_api.create_record_set(
                list_new_records=[local_v4],
                record_type="A",
                record_set_name=setting_dir["current_record_set_A"]
            )
            # logger.logger.info("Record v4 set has been created")
        if not created_v6 and local_v6:
            dns_api.create_record_set(
                list_new_records=[local_v6],
                record_type="AAAA",
                record_set_name=setting_dir["current_record_set_AAAA"]
            )
            # logger.logger.info("Record v6 set has been created")
    except exceptions.ClientRequestException as e:
        if e.status_code == 400:
            logger.logger.error(f"Code:{e.error_code}--{e.error_msg}--Line:{e.__traceback__.tb_lineno}")
        sys.exit(1)
logger.logger.info("End to modify record set")
sys.exit(0)
